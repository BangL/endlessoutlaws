local _first_login_check_original = ElementPlayerSpawner._first_login_check
function ElementPlayerSpawner:_first_login_check(...)
    _first_login_check_original(self, ...)
    if tweak_data and tweak_data.operations and tweak_data.operations.get_all_consumable_raids and
        managers and managers.consumable_missions and managers.consumable_missions._unlock_mission then
        for _, raid in pairs(tweak_data.operations:get_all_consumable_raids()) do
            managers.consumable_missions:_unlock_mission(raid)
        end
    end
end
